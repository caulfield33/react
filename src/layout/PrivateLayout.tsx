import React from 'react';



const PrivateLayout: React.FC = ({children}) => {

    return (
        <React.Fragment>
            {children}
        </React.Fragment>
    )
};

export default PrivateLayout;
