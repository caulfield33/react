import React from 'react';

type Props = {};
const PublicLayout: React.FC<Props> = ({children}) => {

    return (
        <React.Fragment>
            {children}
        </React.Fragment>
    )
};


export default PublicLayout;
