import RequestService from "./request-service";
import {IAuthCredential} from "../models/IAuthCredential";
import {AxiosResponse} from "axios";
import {IUserUpdateInput} from "../models/IUser";

export default class UserService extends RequestService {
    public login = (credential: IAuthCredential): Promise<AxiosResponse> =>
        this.instance.post('user/login', credential)

    public onLoad = (token: string): Promise<AxiosResponse> =>
        this.instance.post('auth/on-load', {token})

    public signUp = (credential: any): Promise<AxiosResponse> =>
        this.instance.post('user/create-account', credential)

    public updateUser = (userDataToUpdate: IUserUpdateInput, userId: string): Promise<AxiosResponse> =>
        this.instance.put('user', userDataToUpdate, {params: {userId}})

    public requestNewPassword = (phone: string): Promise<AxiosResponse> =>
        this.instance.post('user/request-password', {phone})

    public getUser = (): Promise<AxiosResponse> =>
        this.instance.get('user')

}

