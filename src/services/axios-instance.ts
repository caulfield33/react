import axios, {AxiosInstance} from 'axios';
import {env} from "../const/env";
import {APP_AUTH_DATA_REFRESH, APP_AUTH_DATA_TOKEN} from "../const";

const instance: AxiosInstance = axios.create({
    baseURL: env.apiUrl
});

instance.interceptors.request.use(config => {
        const token = localStorage.getItem(APP_AUTH_DATA_TOKEN)
        if (token !== null) config.headers.authorization = 'Bearer ' + token;
        return config;
    },
    error => Promise.reject(error)
);

instance.interceptors.response.use(
    (response) => response,
    async (error) => {
        try {
            const originalRequest = error.config;

            if (error.response && error.response.data) {

                console.log(error.response.data)

                if (error.response.data.message === 'Token is not in white list') {
                    return Promise.reject(error);
                }

                if (error.response.data.message === "jwt expired" && !originalRequest._retry) {

                    const refreshToken = localStorage.getItem(APP_AUTH_DATA_REFRESH)

                    if (refreshToken) {
                        const res = await instance.post('/refresh-token', {refreshToken})

                        if (res.status === 201) {
                            localStorage.setItem(APP_AUTH_DATA_TOKEN, res.data.token)
                            instance.defaults.headers.authorization = `Bearer ${res.data.token}`;
                            return instance(originalRequest);
                        }
                    }
                    originalRequest._retry = true;
                } else {
                    // notification.error({
                    //     message: `${i18n.t('error')}`,
                    //     description: i18n.t(error.response.data.message),
                    //     duration: 5
                    // })

                    return Promise.reject(error);
                }
            }

        } catch (e) {
            console.log(e)
        }

    }
)

export default instance
