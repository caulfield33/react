export const objectToArray = (obj: any): any[] => {
    const temp: any[] = [];

    Object.keys(obj).forEach(key => temp.push(obj[key]))

    return temp
}

export const capitalize = (s: string): string => s.charAt(0).toUpperCase() + s.slice(1);

export const copyToClipboard = (text: string): void => {
    const el = document.createElement('textarea');
    el.value = text;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}


type Picked = any;
type NotPicked = any;
export const splitObject = (object: any, fieldsToPick: string[]): [Picked, NotPicked] => {
    const picked: Picked = {};
    const notPicked: NotPicked = {};

    Object.keys(object).forEach(key => {
        if (!fieldsToPick.includes(key)) {
            picked[key] = object[key]
        } else {
            notPicked[key] = object[key]
        }
    })

    return [picked, notPicked]
}

export const replaceInArray = (array: any[], _id: any, replaceWith: any): any[] => {
    const index = array.findIndex(elm => elm._id === _id)
    array[index] = replaceWith
    return array;
}
