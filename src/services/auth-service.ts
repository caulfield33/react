import RequestService from "./request-service";
import {AxiosResponse} from "axios";

export default class AuthService extends RequestService {
    public logout = (token: string): Promise<AxiosResponse> =>
        this.instance.post('auth/logout', {token})
}

