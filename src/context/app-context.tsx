import React, {createContext, useState} from 'react';

type Props = {
    children: React.ReactNode;
};

export interface Context {
    setContext: (settings: {}) => void;
}

const initialContext: Context = {
    setContext: (): void => {
        throw new Error('setContext function must be overridden');
    },
};

const AppContext = createContext<Context>(initialContext);

const AppContextProvider = ({children}: Props): JSX.Element => {
    const [contextState, _setContext] = useState<Context>(initialContext);

    const setContext = (settings: {}) => {
        const values = {...contextState, ...settings};
        // @ts-ignore
        delete values.setContext

        _setContext((prevState => ({...prevState, ...settings})))
    }

    return (
        <AppContext.Provider value={{...contextState, setContext}}>
            {children}
        </AppContext.Provider>
    );
};

export {AppContext, AppContextProvider};
