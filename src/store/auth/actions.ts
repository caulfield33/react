import {AuthActionTypes} from "./types";
import {IAuthCredential} from "../../models/IAuthCredential";
import {IUser} from "../../models/IUser";


export default class AuthActionsClass {

    static loginRequest = (credential: IAuthCredential) => ({
        type: AuthActionTypes.LOGIN_REQUEST,
        payload: credential
    });

    static loginSuccess = (user: IUser) => ({
        type: AuthActionTypes.LOGIN_SUCCESS,
        payload: user
    })

    static logoutRequest = () => ({
        type: AuthActionTypes.LOGOUT_REQUEST,
    })
    static logoutSuccess = () => ({
        type: AuthActionTypes.LOGOUT_SUCCESS,
    })

    static onLoadRequest = (token: string) => ({
        type: AuthActionTypes.ON_LOAD_REQUEST,
        payload: token
    });

    static noToken = () => ({
        type: AuthActionTypes.NO_TOKEN,
    })

    static onRequestFail = (error: string) => ({
        type: AuthActionTypes.REQUEST_FAIL,
        payload: error
    })
}
