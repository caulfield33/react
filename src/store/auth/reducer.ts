import {Reducer} from "redux";
import {AuthActionTypes, AuthState} from "./types";

export const initialState: AuthState = {
    user: null,
    loading: true
};

const reducer: Reducer<AuthState> = (state = initialState, action) => {
    switch (action.type) {
        case AuthActionTypes.NO_TOKEN: {
            return {...state, loading: false};
        }
        case AuthActionTypes.LOGIN_REQUEST: {
            return {...state, loading: true};
        }
        case AuthActionTypes.LOGIN_SUCCESS: {
            return {...state, loading: false, user: action.payload};
        }
        case AuthActionTypes.REQUEST_FAIL: {
            return {...state, loading: false};
        }
        default: {
            return state;
        }
    }
};

export {reducer as AuthReducer};
