import {IUser} from "../../models/IUser";

export interface AuthState {
    readonly loading: boolean;
    readonly user: IUser | null;
}

export enum AuthActionTypes {

    LOGIN_REQUEST = "@@auth/LOGIN_REQUEST",
    LOGIN_SUCCESS = "@@auth/LOGIN_SUCCESS",

    SIGN_UP_REQUEST = "@@auth/SIGN_UP_REQUEST",
    SIGN_UP_SUCCESS = "@@auth/SIGN_UP_SUCCESS",

    LOGOUT_SUCCESS = "@@auth/LOGOUT_SUCCESS",
    LOGOUT_REQUEST = "@@auth/LOGOUT_REQUEST",

    NO_TOKEN = "@@auth/NO_TOKEN",

    ON_LOAD_REQUEST = "@@auth/ON_LOAD_REQUEST",

    REQUEST_FAIL = "@@auth/REQUEST_FAIL",
}
