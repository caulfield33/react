import {AuthActionTypes} from "./types";
import {APP_AUTH_DATA_REFRESH, APP_AUTH_DATA_TOKEN} from "../../const";
import {IAuthCredential} from "../../models/IAuthCredential";
import AuthActionsClass from "./actions";
import {takeEvery, put} from 'redux-saga/effects';
import UserService from "../../services/user-service";
import {push} from "react-router-redux";
import AuthService from "../../services/auth-service";

const userService = new UserService();
const authService = new AuthService();

function* loginWorker(action: { type: string, payload: IAuthCredential }) {
    try {
        const response = yield userService.login(action.payload);
        localStorage.setItem(APP_AUTH_DATA_TOKEN, response.data.token);
        localStorage.setItem(APP_AUTH_DATA_REFRESH, response.data.refreshToken);
        yield put(AuthActionsClass.loginSuccess(response.data.user));
        yield put(push(`/dashboard`));
    } catch (e) {
        yield put(AuthActionsClass.onRequestFail(e.message))
    }
}

function* onLoadUserWorker(action: { type: string, payload: string }) {
    try {
        const response = yield userService.onLoad(action.payload);
        localStorage.setItem(APP_AUTH_DATA_TOKEN, response.data.token);
        localStorage.setItem(APP_AUTH_DATA_REFRESH, response.data.refreshToken);
        yield put(AuthActionsClass.loginSuccess(response.data.user))
    } catch (e) {
        yield put(AuthActionsClass.onRequestFail(e.message))
    }
}

function* logoutWorker() {
    try {
        yield authService.logout(localStorage.getItem(APP_AUTH_DATA_TOKEN) as string);
        localStorage.removeItem(APP_AUTH_DATA_TOKEN);
        localStorage.removeItem(APP_AUTH_DATA_REFRESH);
        yield put(AuthActionsClass.logoutSuccess());
        yield put(push(`/`));
    } catch (e) {
        yield put(AuthActionsClass.onRequestFail(e.message))
    }

}

export function* watchAuth() {
    yield takeEvery(AuthActionTypes.LOGIN_REQUEST, loginWorker);
    yield takeEvery(AuthActionTypes.ON_LOAD_REQUEST, onLoadUserWorker);
    yield takeEvery(AuthActionTypes.LOGOUT_REQUEST, logoutWorker);
}

const authWatchers: any = [
    watchAuth()
];

export default authWatchers
