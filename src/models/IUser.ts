import {IDatabaseRecord} from "./IDatabaseRecord";


export interface IUser extends IDatabaseRecord {
    name: string;
    email: string;
    password: string;
    role: string;
    permission: string;
}

export interface IUserInput {
    name: string;
    email: string;
    password: string;
    role: string;
    permission: string;
}

export interface IUserUpdateInput {
    name?: string;
    email?: string;
    role?: string;
    permission?: string;
}
