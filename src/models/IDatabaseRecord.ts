export interface IDatabaseRecord {
    _id: string;
    cratedAt: Date;
    updatedAt: Date;
}
