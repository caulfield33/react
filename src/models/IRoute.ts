import React from "react";

export interface IRoute {
    isPublic?: boolean;
    component: React.FC<any>;
    layout:  React.FC;
    path: string;
    allowedForLogged: boolean;
    exec?: boolean;
    requiredRoles?: string[]
    children?: IRoute[]
}
