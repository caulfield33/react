import i18n from 'i18next'
import Backend from 'i18next-xhr-backend'
import {initReactI18next} from 'react-i18next'
import * as moment from 'moment';
import 'moment/locale/en-gb'
import {APP_LANG, languages} from "./const";

const userLang = navigator.language || (navigator as any).userLanguage;

let appLang = 'en'

const memoLang = localStorage.getItem(APP_LANG)
if (memoLang) {
    appLang = memoLang;
} else {
    if (userLang.includes('-')) {
        const [lang] = userLang.split('-')

        if (languages.includes(lang)) {
            appLang = lang
        }
    }
}


moment.locale(appLang)
localStorage.setItem(APP_LANG, appLang as string)
i18n
    .use(Backend)
    .use(initReactI18next)
    .init({
        lng: appLang,
        backend: {
            loadPath: '/assets/i18n/{{ns}}/{{lng}}.json'
        },
        fallbackLng: 'en',
        debug: false,
        ns: ['translations'],
        defaultNS: 'translations',
        keySeparator: false,
        react: {
            wait: true
        }
    })

export default i18n
