export interface IEnv {
    apiUrl: string
}

export const env: IEnv = {
    apiUrl: 'http://localhost:5000/api/v1/'
}
