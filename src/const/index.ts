export const APP_AUTH_DATA_TOKEN = 'APP_AUTH_DATA_TOKEN'
export const APP_AUTH_DATA_REFRESH = 'APP_AUTH_DATA_TOKEN_USER'
export const APP_LANG = 'APP_LANG'

export const languages = ['ua', 'ru', 'it', 'pl', 'en']
