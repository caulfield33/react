import React from 'react';
import styled from "styled-components";
import {Link} from "react-router-dom";

const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
`

type Props = {}

const Home: React.FC<Props> = () => {

    return (
        <Wrapper>
            <h1>Home WIP</h1>
            <h1><Link to={'/auth?login'}>auth</Link></h1>
        </Wrapper>
    )
}


export default Home;

