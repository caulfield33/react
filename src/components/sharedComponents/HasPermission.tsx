import React, {ReactElement} from "react";
import {useSelector} from "react-redux";
import {IApplicationState} from "../../models/IApplicationState";
import {IUser} from "../../models/IUser";

type Props = {
    children: ReactElement;
    requiredRoles: string[];
    ifNoPermission?: ReactElement;
}

const HasPermission: React.FC<Props> = React.memo(({
                                                       children,
                                                       requiredRoles,
                                                       ifNoPermission
                                                   }) => {

    const user: IUser | null = useSelector((state: IApplicationState) => state.auth.user)

    if (user && requiredRoles.includes(user.role)) {
        console.log('has permission')
        return children
    } else {
        if (ifNoPermission) {
            console.log('no permission, ifNoPermission')
            return ifNoPermission
        }
    }
    // console.log('no permission, no ifNoPermission')
    return  null
})

export default HasPermission;
