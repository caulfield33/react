import React from 'react';
import styled from "styled-components";
import {useTranslation} from "react-i18next";

const Wrapper = styled.div``;

type Props = {}

const Loader: React.FC<Props> = () => {
    const {t} = useTranslation()

    return  (
        <Wrapper>
            {t('Loading')}
        </Wrapper>
    );
}

export default Loader
