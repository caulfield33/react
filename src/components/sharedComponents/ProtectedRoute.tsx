import {Redirect, Route, RouteComponentProps} from 'react-router-dom';
import React from "react";
import {useSelector} from "react-redux";
import {IApplicationState} from "../../models/IApplicationState";
import {IRoute} from "../../models/IRoute";
import PublicLayout from "../../layout/PublicLayout";
import Home from "../Home/Home";
import PrivateLayout from "../../layout/PrivateLayout";
import Dashboard from "../Dashboard/Dashboard";
import Loader from "./Loader";

export const appRoutes: IRoute[] = [
    {
        component: Home,
        path: '/',
        layout: PublicLayout,
        isPublic: true,
        allowedForLogged: true,
    },
    {
        component: Dashboard,
        path: '/dashboard',
        layout: PrivateLayout,
        isPublic: false,
        allowedForLogged: true,
        children: []
    }
]

interface Props {
    Component: React.FC<RouteComponentProps>;
    Layout: React.FC;
    isPublic?: boolean;
    allowedForLogged: boolean;
    path: string;
    exact?: boolean;
    requiredRoles?: string[]
}

const ProtectedRoute = ({Layout, Component, path, exact = false, isPublic, allowedForLogged}: Props): JSX.Element => {

    const {loading, user} = useSelector((state: IApplicationState) => ({
        loading: state.auth.loading,
        user: state.auth.user,
    }))

    return (
        <Route
            exact={exact}
            path={path}
            render={(props: RouteComponentProps) => {

                if (loading && user === null) return <Loader/>

                const content = (
                    <Layout>
                        <Component {...props} />
                    </Layout>
                )

                //FIXME

                // const hasPermission = !requiredRoles ? true : authData && authData.user.roles.some(item => requiredRoles.includes(item))

                const hasPermission = true;

                if (isPublic && (allowedForLogged ? true : !(user && user && hasPermission))) return content

                return user && user ? !hasPermission ?
                    null : content : (
                    <Redirect
                        to={{
                            pathname: '/auth',
                            search: 'login'
                        }}
                    />
                )
            }}
        />
    )
}


export default ProtectedRoute;
