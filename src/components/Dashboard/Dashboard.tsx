import React from 'react';
import styled from "styled-components";
import {useDispatch} from "react-redux";
import AuthActionsClass from "../../store/auth/actions";

const Wrapper = styled.div`
    width: fit-content;
    margin: var(--large);
    display: flex;
    flex-direction: column;
    width: auto;
    padding: var(--small);
`;

type Props = {}
const Dashboard: React.FC<Props> = () => {
    const dispatch = useDispatch();

    const logoutHandler = () => dispatch(AuthActionsClass.logoutRequest());

    return (
        <Wrapper>
            <h1>Dashboard WIP</h1>
            <button onClick={logoutHandler}>logout</button>
        </Wrapper>
    )
};

export default Dashboard;


