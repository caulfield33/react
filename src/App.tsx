import React, {useEffect} from 'react';
import {ConnectedRouter} from "connected-react-router";
import {useDispatch} from "react-redux";
import {history} from "./store";
import {Switch} from "react-router";
import ProtectedRoute, {appRoutes} from "./components/sharedComponents/ProtectedRoute";
import {IRoute} from "./models/IRoute";
import {AppContextProvider} from "./context/app-context";
import {APP_AUTH_DATA_TOKEN} from "./const";
import AuthActionsClass from "./store/auth/actions";

const App = () => {

    const dispatch = useDispatch()

    useEffect(() => {
        const token = localStorage.getItem(APP_AUTH_DATA_TOKEN)

        if (token) {
            dispatch(AuthActionsClass.onLoadRequest(token))
        } else {
            dispatch(AuthActionsClass.noToken())
        }

        // eslint-disable-next-line
    }, [])

    const routes: any = []

    appRoutes.forEach((route: IRoute, index: number) => {
        routes.push((
            <ProtectedRoute
                key={index}
                Layout={route.layout}
                isPublic={route.isPublic}
                allowedForLogged={route.allowedForLogged}
                path={route.path}
                requiredRoles={route.requiredRoles}
                Component={route.component}
                exact/>
        ))

        if (route.children) {
            route.children.forEach((childRoute: IRoute, index: number) => {
                routes.push((
                    <ProtectedRoute
                        key={index}
                        Layout={childRoute.layout}
                        isPublic={childRoute.isPublic}
                        allowedForLogged={childRoute.allowedForLogged}
                        path={route.path + childRoute.path}
                        requiredRoles={childRoute.requiredRoles}
                        Component={childRoute.component}
                        exact/>
                ))
            })
        }
    })

    return (
        <ConnectedRouter history={history}>
            <AppContextProvider>
                <Switch>
                    {routes}
                </Switch>
            </AppContextProvider>
        </ConnectedRouter>
    );
}

export default App;
